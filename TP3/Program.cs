﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP3.BE;

namespace TP3
{
    class Program
    {
        private static List<Disco> _discos = new List<Disco>();
        static void Main()
        {
            Console.WriteLine("Bienvenido al creador automatizado de Discos");
            Console.WriteLine("Por favor, seleccione una opción que desea crear.");
            Console.WriteLine("1 - DVD");
            Console.WriteLine("2 - BlueRay");
            Console.WriteLine("3 - Listar discos creados");

            decimal tipoDisco = 0;
            decimal tipoCapa = 0;

            ValidarPrimerEntrada(out tipoDisco, "Ingrese valores 1, 2 o 3 para la opción deseada");

            if (tipoDisco == 1)
            {
                Console.WriteLine("Se creará un DVD");
                Console.WriteLine("Por favor, seleccione una opción de las Capas que desea tener");
                Console.WriteLine("1 - Simple Capa");
                Console.WriteLine("2 - Simple Capa");

                ValidarEntrada(out tipoCapa, "Ingrese valores 1 o 2 para la opción deseada");

                DVD dvd = new DVD();

                if (tipoCapa == 1)
                {
                    dvd.CalcularSimpleCapa(Decimal.Parse("4,7"), 5);
                }
                else
                {
                    dvd.CalcularDobleCapa(Decimal.Parse("8.5") , 8);
                }

                _discos.Add(dvd);

                Console.WriteLine("Disco creado con éxito!");
            }
            else if(tipoDisco == 2)
            {
                Console.WriteLine("Se creará un BlueRay");
                Console.WriteLine("Por favor, seleccione una opción de las Capas que desea tener");
                Console.WriteLine("1 - Simple Capa");
                Console.WriteLine("2 - Simple Capa");

                ValidarEntrada(out tipoCapa, "Ingrese valores 1 o 2 para la opción deseada");

                BluRay blueRay = new BluRay();

                if (tipoCapa == 1)
                {
                    blueRay.CalcularSimpleCapa(40, 50);
                }
                else
                {
                    blueRay.CalcularDobleCapa(25, 20);
                }

                _discos.Add(blueRay);

                Console.WriteLine("Disco creado con éxito!");
            }
            else
            {
                foreach (var item in _discos)
                {
                    if(item.GetType().Equals(typeof(DVD)))
                    {
                        Console.WriteLine("DVD:");
                        Console.WriteLine($"     Capacidad: {item.Capacidad}");
                        Console.WriteLine($"     Precio: {item.Precio}");
                    }
                    else
                    {
                        Console.WriteLine("BlueRay:");
                        Console.WriteLine($"     Capacidad: {item.Capacidad}");
                        Console.WriteLine($"     Precio: {item.Precio}");
                    }
                }

                Console.WriteLine($"Cantidad total: {_discos.Count}");
            }


            Console.WriteLine("     ");
            Console.WriteLine("     ");
            Main();
        }

        private static void ValidarPrimerEntrada(out decimal valor, string mensaje)
        {
            while (true)
            {
                if (Decimal.TryParse(Console.ReadLine(), out valor))
                {
                    if ((valor == 1) || (valor == 2) || (valor == 3))
                        break;
                }

                Console.WriteLine(mensaje);
            }
        }
        private static void ValidarEntrada(out decimal valor, string mensaje)
        {
            while (true)
            {
                if (Decimal.TryParse(Console.ReadLine(), out valor))
                {
                    if ((valor == 1) || (valor == 2) || (valor == 3))
                        break;
                }

                Console.WriteLine(mensaje);
            }
        }
    }
}
