﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TP3.BE
{
    public class DVD : Disco, ICapa
    {
        public void CalcularSimpleCapa(decimal capacidad, decimal precio)
        {
            Capacidad = capacidad;
            Precio = precio;
        }

        public void CalcularDobleCapa(decimal capacidad, decimal precio)
        {
            Capacidad = capacidad;
            Precio = precio;
        }
    }
}