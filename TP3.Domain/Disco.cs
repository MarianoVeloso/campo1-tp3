﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TP3.BE
{
    public abstract class Disco
    {
        private decimal _capacidad;

        public decimal Capacidad
        {
            get { return _capacidad; }
            set { _capacidad = value; }
        }
        private decimal _precio;

        public decimal Precio
        {
            get { return _precio; }
            set { _precio = value; }
        }
    }
}