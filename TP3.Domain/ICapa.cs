﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TP3.BE
{
    public interface ICapa
    {
        void CalcularSimpleCapa(decimal capacidad, decimal precio);
        void CalcularDobleCapa(decimal capacidad, decimal precio);
    }
}